import mongo_client
class EmailManager(object):
    __instance = None

    @staticmethod
    def inst():
        if EmailManager.__instance == None:
            EmailManager.__instance = EmailManager()
        return EmailManager.__instance

    #single call check
    def __init__(self):
        self.email_list = mongo_client.get_emails()
