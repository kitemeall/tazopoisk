class AvitoItem(object):


    def __init__(self, url, name, price="Not set", descr="Not set"):
        self.url = url
        self.name = name
        self.price = price
        self.img_files = []
        self.descr = descr


    def __str__(self):
        return self.name + "\n" + self.price + "\n" + self.url + "\n" + self.descr

