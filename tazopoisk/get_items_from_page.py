import requests
from bs4 import BeautifulSoup
from avito_item import AvitoItem
def parse(item_html):
	title = item_html.find("h3")
	price = item_html.find("div",{"class": "about"}).find(text=True)
	name = title.text
	url = "https://www.avito.ru" + title.find("a")["href"]
	return AvitoItem(url, name, price)

def get_items_from_page(link):
	r = requests.get(link)
	html_doc = r.text

	soup = BeautifulSoup(html_doc, 'html.parser')
	items = soup.findAll("div", { "class" : "item" })
	items_list = []
	for item in items:
		parsed_item = parse(item)
		items_list.append(parsed_item)
	return items_list

