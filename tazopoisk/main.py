#!/usr/bin/python3
import get_items_from_page
import mongo_client
import mail_sender
import datetime
import item_processor


search_urls = mongo_client.get_urls()
for search_url in search_urls:
	items = get_items_from_page.get_items_from_page(search_url)
	for item in items:
		if mongo_client.check_that_new(item):
			item_processor.process_item(item)
			print(str(item) + "\n" + datetime.datetime.now().strftime("%d.%m.%Y %H:%M"))
			mail_sender.notify(item)
			mongo_client.insert_to_db(item)


