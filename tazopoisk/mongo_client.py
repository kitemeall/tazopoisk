from pymongo import MongoClient


client = MongoClient("mongodb://taz:taz@ds163667.mlab.com:63667/tazopoisk")
db = client.tazopoisk


def check_that_new(item):
	items = db.items
	if items.find_one({'url' : item.url}) == None:
		return True
	return False


def insert_to_db(item):
	items = db.items
	items.insert_one({'url' : item.url})


def get_emails():
	emails = db.emails
	emailList = []
	cursor = emails.find()
	for document in cursor:
		emailList.append(document["email"])
	return emailList


def get_urls():
	urls = db.urls
	urlList = []
	cursor = urls.find()
	for document in cursor:
		urlList.append(document["url"])
	return urlList


if __name__ == "__main__":
	print(get_emails())
	print(get_urls())
