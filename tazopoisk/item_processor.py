import requests
from bs4 import BeautifulSoup
import re
import hashlib

md5 = hashlib.md5()
prefix = "images/"


def download_image(url, filename):
    r = requests.get(url)
    with open(prefix + filename, 'wb') as outfile:
        outfile.write(r.content)


def process_images(item, soup):
    imgs = soup.findAll('div', {"data-url": re.compile('1280')})
    img_urls = []
    for i in imgs:
        img_urls.append("http:" + i["data-url"])

    for img_url in img_urls:
        md5.update(img_url.encode('utf-8'))
        image_name = md5.hexdigest() + '.jpg'
        download_image(img_url, image_name)
        item.img_files.append(prefix + image_name)
def process_description(item, soup):
    descr = soup.find("div", {"class": "item-description"})
    item.descr = str(descr)

def process_item(item):
    r = requests.get(item.url)
    html_doc = r.text
    soup = BeautifulSoup(html_doc, 'html.parser')
    process_description(item,soup)
    process_images(item, soup)


if __name__ == "__main__":
    from avito_item import AvitoItem
    item = AvitoItem("https://www.avito.ru/sankt-peterburg/avtomobili/vaz_2115_samara_2002_890312851", "test")
    process_item(item)
    print(item);
    print(item.img_files)
