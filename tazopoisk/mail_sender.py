import yagmail
from email_addr_manager import EmailManager


def send_mail(item, to):
    yagmail.register('tazopoisk@gmail.com', 'findmytaz')
    yag = yagmail.SMTP('tazopoisk@gmail.com')
    contents = [str(item)]
    for image in item.img_files:
        contents.append(image)
    header = item.name + " "+item.price
    yag.send(to, header, contents )


def notify(item):
    for email in EmailManager.inst().email_list:
        send_mail(item, email)



